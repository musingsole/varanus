from time import sleep
from edaphic.wipy_utilities import simple_connect, get_timestamp, enable_ntp
from varanus import Varanus
from machine import reset


print("Executing main")
simple_connect("noobiemcfoob", "n00biemcfoob")
print("WiFi connected. Enabling NTP")
enable_ntp()
print("Building varanus")
varanus = Varanus()
print("Initial read: {}".format(varanus.murd_memory()))

print("Beginning execution")
while True:
    print("Starting       : {}".format(get_timestamp()))
    varanus.update()
    sleep(0.5)

