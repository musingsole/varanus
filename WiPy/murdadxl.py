from machine import ADC, Pin
import json

from murd_client import MurdMemory
from edaphic.wipy_utilities import get_timestamp


class MurdAdxl:
    def __init__(self,
                 name="adxl",
                 st_pin="P19",
                 z_pin="P18",
                 y_pin="P17",
                 x_pin="P16"):
        self.name = name
        self.adc = ADC()
        self.adc.vref(1240)
        self.st = Pin(st_pin, mode=Pin.OUT)
        self.z = self.adc.channel(pin=z_pin, attn=ADC.ATTN_11DB)
        self.y = self.adc.channel(pin=y_pin, attn=ADC.ATTN_11DB)
        self.x = self.adc.channel(pin=x_pin, attn=ADC.ATTN_11DB)
        self.one_g = 0.35
        self.baseline = 1.5

    @staticmethod
    def rotary_read(axes=[], cycles=1):
        rotary = [[] for axis in axes]
        for i in range(cycles):
            for axis_index, axis in enumerate(axes):
                rotary[axis_index].append(axis())
        for i in range(len(axes)):
            rotary[i] = (sorted(rotary[i])[int(cycles / 2)] / 4096) * 3.3 - 1.65
        return rotary

    def read(self, cycles=100):
        axes = [self.x, self.y, self.z]
        return self.rotary_read(axes, cycles=cycles)

    def murd_memory(self, cycles=100):
        ts = get_timestamp()
        memory = MurdMemory(
            ROW=self.name,
            COL=ts,
            CYCLES=cycles,
            READING=json.dumps(self.read())
        )

        return memory
