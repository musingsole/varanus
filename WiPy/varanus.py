import json
from edaphic.wipy_utilities import get_timestamp
from urequests import put
from uwebsockets import connect
from murd_client import MurdMemory
from murdadxl import MurdAdxl

ws_url = "wss://6zlsr9hpq3.execute-api.us-east-1.amazonaws.com/prod"

murd_url = "https://uym92tuf5i.execute-api.us-east-1.amazonaws.com/prod"


class Varanus:
    def __init__(self,
                 name="varanus",
                 ws_url=ws_url):
        self.adxl = MurdAdxl(
            name="adxl",
            st_pin="P20",
            z_pin="P18",
            y_pin="P17",
            x_pin="P16")
        # self.adxl_right = MurdAdxl(
        #     name="adxl_left",
        #     st_pin="P20",
        #     z_pin="P13",
        #     y_pin="P14",
        #     x_pin="P15")
        # self.ws = connect(ws_url)

    def read(self, cycles=50):
        for i in range(cycles):
            left_read = self.adxl.read(cycles)
            right_read = self.adxl_right.read(cycles)
        return {self.adxl_left.name: left_read,
                self.adxl_right.name: right_read}
    
    def murd_memory(self, cycles=50):
        ts = get_timestamp()
        return MurdMemory(
            ROW="varanus",
            COL=ts,
            CYCLES=cycles,
            READING=self.adxl.read(cycles))

    def update(self, cycles=50):
        mems = [self.murd_memory(cycles) for i in range(cycles)]
        resp = put(murd_url + "/murd",
                   json={"mems": json.dumps(mems)})
        if resp.status_code != 200:
           print("Server Error: {}".format(resp.status_code))
           print(resp.content)
           sleep(5)
           reset()
        print("MurdUpdate succcessful")

    def update_ws(self, cycles=50):
        mem = self.murd_memory(cycles)
        packet = {"route": "update",
                  "mems": [mem],
                  "identifier": "varanus"}
        self.ws.send(json.dumps(packet))

