class I2CRegister:
    def __init__(self, name, address, bytes=1):
        self.name = name
        self.address = address
        self.bytes = bytes

        self.pins = {i: 0 for i in self.bits}

    def __getitem__(self, index):
        if isinstance(index, slice):
            keys = list(self.pins.keys())[index]
            return [self.pins[key] for key in keys]
        else:
            return self.pins[index]

    def __iter__(self):
        for i in range(self.size * 8):
            yield self.pins[i]

    @property
    def size(self):
        return self.bytes * 8

    @property
    def bits(self):
        return list(range(self.size))


class LSM9DS1TR:
    ACCEL_GYRO_ADDRESS = 0x6B
    MAG_ADDRESS = 0x1E

    def __init__(self,
                 sda="P9",
                 scl="P10"):
        # Accelerometer Values
        self.activity = {
            "mode": 0,
            "threshold": 0,
            "inactivity_duration": 0
        }

        self.interrupts = {
            "generator_config": {
                "AND_OR": 0,
                "6D": 0,
                "HI_Z": 0,
                "LO_Z": 0,
                "HI_Y": 0,
                "LO_Y": 0,
                "HI_X": 0,
                "LO_X": 0
            },
            "thresholds": {
                "x": 0,
                "y": 0,
                "z": 0
            },
            "wait": 0,
            "wait_duration": 0,
            "interrupt_1_enables": {
                "gyro": 0,
                "accel": 0,
                "fss5": 0,
                "overrun": 0,
                "fifo": 0,
                "boot": 0,
                "gyro_data": 0,
                "accel_data": 0
            },
            "interrupt_2_enables": {
                "gyro": 0,
                "accel": 0,
                "fss5": 0,
                "overrun": 0,
                "fifo": 0,
                "boot": 0,
                "gyro_data": 0,
                "accel_data": 0
            }
        }

        self.reference = 0

        self.who_am_i = 0x68
